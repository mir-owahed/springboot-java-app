FROM eclipse-temurin:11

WORKDIR /

RUN mvn clean package

ADD target/spring-boot-web.jar spring-boot-web.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/spring-boot-web.jar"]
